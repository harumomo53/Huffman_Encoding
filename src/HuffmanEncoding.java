import java.io.*;
import java.nio.Buffer;
import java.util.*;

/*Takes in a file name, compresses the file, and outputs the compressed file with a 
 * codemap header */
public class HuffmanEncoding {

    protected String myTarget; // file to be compressed
    protected String myDestination; // file to be decompressed

    protected static FileCharIterator myIter;
    protected static FileCharIterator inputStream;
    protected static Map<String, Integer> strFreq;

    protected static BinaryTree myBT;
    protected static HashMap<String, String> myChrToCodeMap;

    static int total = 0;

    public static void main(String[] args) {
        encode1("src/sample_files/smallFile.txt", "src/outputEncode.txt");

        //decode("src/outputEncode.txt", "src/outputDecode.txt");

        //decode("src/sample_files/smallFile.txt.huffman", "src/outputDecode.txt");

        // decode("src/sample_files/lastquestion.txt.huffman", "src/outputDecode.txt");
    }

    public HuffmanEncoding(String target, String destination) {
        // myIter.next() returns string
        myIter = new FileCharIterator(target);
        inputStream = new FileCharIterator(target);
        strFreq = new TreeMap<>();
        myTarget = target;
        myDestination = destination;
    }


    public void countFreq() {
        while(myIter.hasNext()) {
            String myNext = myIter.next();
            if (!strFreq.containsKey(myNext)) {
                strFreq.put(myNext, 1);
            } else {
                int count = strFreq.get(myNext);
                strFreq.put(myNext, count+1);
            }
        }
    }

    public void codewordMap(HashMap<String, String> chrMap) {
        // Create Codeword mapping header
        StringBuilder strToRtn = new StringBuilder();
        for (Map.Entry<String, String> entry : chrMap.entrySet()) {
            strToRtn.append(entry.getKey());
            strToRtn.append(",");
            strToRtn.append(entry.getValue());
            strToRtn.append("\n");
        }
        strToRtn.append("\n");

        this.writeString(strToRtn.toString());
        //return strToRtn.toString();
    }

    public void writeString(String codeword) {
        for (int i = 0; i < codeword.length(); i++) {
            String str = HuffmanEncoding.AsciiToBinary(codeword.substring(i, i+1));
            FileOutputHelper.writeBinStrToFile(str, myDestination);
        }
    }


    private static String AsciiToBinary(String asciiString) {
        /*
         * From StackOverflow, helper to convert Ascii to Binary
         */
        byte[] bytes = asciiString.getBytes();
        StringBuilder binary = new StringBuilder();
        for (byte b : bytes)
        {
            int val = b;
            for (int i = 0; i < 8; i++)
            {
                binary.append((val & 128) == 0 ? 0 : 1);
                val <<= 1;
            }
        }
        return binary.toString();
    }

    // For encode1()
    public void outputEncode(int iterType, HashMap<String, String> chrMap) {
        /**
         * This method reads in every word (which is 1 byte long) in the target file
         * and convert it to bits (based on the Huffman Encoding tree)
         */

        // Reading each character in the file using FileCharIterator
        StringBuilder strToRtn = new StringBuilder();
        if (iterType == 0) {
            String next;
            while (inputStream.hasNext()) {
                next = inputStream.next();
                // Compress!
                strToRtn.append(chrMap.get(next));
            }
        }
        strToRtn.append(chrMap.get("EOF"));

        writeToFile(strToRtn);
    }

    // For encode2()
    public void outputEncode(int iterType, FileFreqWordsIterator iter, HashMap<String, String> chrMap) {
        StringBuilder strToRtn = new StringBuilder();
        String next;
        if (iterType == 1) {
            while (iter.hasNext()) {
                next = iter.next();
                strToRtn.append(chrMap.get(next));
            }
        }
        strToRtn.append(chrMap.get("EOF"));
        writeToFile(strToRtn);
    }

    public void writeToFile(StringBuilder strToRtn) {
        for (int i = 0; i < strToRtn.length(); i += 8) {
            String str = "";
            // if strToRtn is shorter than i+8
            if (i + 8 > strToRtn.length()) {
                // write down all the string
                str = strToRtn.substring(i, strToRtn.length());
                // and fill 0s afterwards
                while (str.length() < 8) {
                    str += "0";
                }
            } else {
                // make this in "bits" and write it to the destination file
                str = strToRtn.substring(i, i+8);
            }
            FileOutputHelper.writeBinStrToFile(str, myDestination);
        }
    }


    public static void encode1(String target, String destination) {
        /**
         * Encode version 1 : using normal FileCharIterator
         */
        HuffmanEncoding huffman = new HuffmanEncoding(target, destination);
        // count characters' frequencies
        huffman.countFreq();
        // construct a huffman encoding tree
        BinaryTree bt = new BinaryTree(strFreq);
        bt.convertMap();
        BinaryTree.TreeNode myRoot = bt.generateTree();
        //bt.print();
        HashMap<String, String> chrMap = bt.generateMapping();

        // Output phase
        huffman.codewordMap(chrMap);
        huffman.outputEncode(0, chrMap);
        gatherStrCount(new File("src/outputEncode.txt"));

    }


    public static void encode2(String target, String destination, int n) {
        /**
         * Encode version 2 : using customized FireFreqWordsIterator (more efficient way of encoding)
         */
        HuffmanEncoding huffman = new HuffmanEncoding(target, destination);
        FileFreqWordsIterator ffwIter = new FileFreqWordsIterator(target, n); // will have finalMapToUse
        huffman.codewordMap(ffwIter.chrMap);
        huffman.outputEncode(1, ffwIter, ffwIter.chrMap);
    }

    private static String gatherStrCount(File tempZip) {
        StringBuilder sb = new StringBuilder();
        try {
            FileReader fr = new FileReader(tempZip);
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();
            while (line != null) {
                sb.append(line.toString());
                //System.out.println(line.toString());
                line = br.readLine();
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }


    public static void decode(String target, String destination) {
        try {
            HuffmanEncoding huffman = new HuffmanEncoding(target, destination);
            HashMap<String, String> codeMap = new HashMap<>();
            FileReader fr = new FileReader(target);
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();

            while (!line.isEmpty()) {
                for (int i = 0; i < line.length() + 1; i++) {
                    myIter.next();
                }
                String[] splitted = line.split(",");
                codeMap.put(splitted[1], splitted[0]);
                line = br.readLine();
            }

            myIter.next(); // End of codeMap
            br.close();

            String str = "";
            String ch = "";

            // Content
            System.out.println(codeMap);
            while (myIter.hasNext()) {
                String curr = myIter.next();
                System.out.println("curr : " + curr);
                for (int i = 0; i < curr.length(); i++) {
                    ch += curr.substring(i, i+1);
                    if (codeMap.containsKey(ch)) {
                        if (codeMap.get(ch).equals("EOF")) {
                            FileOutputHelper.writeBinStrToFile(str, destination);
                            return;
                        }
                        str += codeMap.get(ch);
                        //System.out.println("str: " +str);
                        ch = "";
                    }
                }
                FileOutputHelper.writeBinStrToFile(str, destination);
                str = "";
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

/**
 * Created by EunjinCho on 2016. 3. 22..
 */
public class FileFreqWordsIterator implements Iterator<String> {


    protected String myInputFileName;
    protected int myN;
    protected Map<String, Integer> freqWordsMap;
    protected Map<String, Integer> freqCharsMap;
    protected Map<String, Integer> nFreqWordsMap;
    protected Map<String, Integer> finalMapToUse;
    HashMap<String, String> chrMap;
    protected FileCharIterator iter;
    protected FileCharIterator iter2;
    protected ArrayList<ArrayList<String>> wordList;
    protected ArrayList<String> word;
    protected String myNext;
    protected String wordCheck = "";
    protected int hasSpace = 0;
    protected int hasNewLine = 0;
    protected ArrayList<String> toAdd;
    private int h = 0;
    private boolean started = false;
    private boolean lastChars = false;


    public FileFreqWordsIterator(String inputFileName, int n) {

        // Instance variables naming
        myInputFileName = inputFileName;
        myN = n;
        freqWordsMap = new TreeMap<>();
        freqCharsMap = new TreeMap<>();
        nFreqWordsMap = new TreeMap<>();
        finalMapToUse = new TreeMap<>();
        chrMap = new HashMap<String, String>(); // final map to use
        word = new ArrayList<>();
        wordList = new ArrayList<>();
        iter = new FileCharIterator(inputFileName);
        iter2 = new FileCharIterator(inputFileName);
        toAdd = new ArrayList<>();

        // Create mapping of n-# of frequent words and the rest of characters
        collectWords();
        countFreqWords();
        getNFreqWords();
        handleRest();
        // Encode
        BinaryTree bt = new BinaryTree(finalMapToUse);
        bt.convertMap();
        BinaryTree.TreeNode myRoot = bt.generateTree();
        chrMap = bt.generateMapping(); // final encode map to use

    }

    private void handleRest() {
        for (Map.Entry<String, Integer> entry: freqWordsMap.entrySet()) {
            String key = entry.getKey();
            for (int i = 0; i < key.length(); i += 8) {
                String subChar = key.substring(i, i+8);
                if (freqCharsMap.containsKey(subChar)) {
                    freqCharsMap.put(subChar, freqCharsMap.get(subChar)+1);
                } else {
                    freqCharsMap.put(subChar, 1);
                }
            }
        }
        // Eventually combine both nFreqWordsMap and freqCharsMap
        finalMapToUse.putAll(nFreqWordsMap);
        finalMapToUse.putAll(freqCharsMap);
        if (hasSpace > 0) {
            finalMapToUse.put("00100000", hasSpace);
        } if (hasNewLine > 0) {
            finalMapToUse.put("00001010", hasNewLine);
        }
        System.out.println(finalMapToUse);
    }

    private Map.Entry<String,Integer> getLargest(Map<String, Integer> newFreq) {
        int max = Integer.MIN_VALUE;
        Map.Entry<String, Integer> max_entry = null;
        for (Map.Entry<String, Integer> entry : newFreq.entrySet()) {
            if (max < entry.getValue()) {
                max = entry.getValue();
                max_entry = entry;
            }
        }
        return max_entry;
    }

    private void getNFreqWords() {
        nFreqWordsMap = new TreeMap<>();
        for (int i = 0; i < myN; i++) {
            Map.Entry<String, Integer> smallest = getLargest(freqWordsMap);
            nFreqWordsMap.put(smallest.getKey(), smallest.getValue());
            freqWordsMap.remove(smallest.getKey());
        }
    }

    private void collectWords() {
        while(iter.hasNext()) {
            myNext = iter.next();
            while (!myNext.equals("00100000") && !myNext.equals("00001010") && !myNext.isEmpty()) {
                word.add(myNext);
                myNext = iter.next();
            }
            if (myNext.equals("00100000")) {
                hasSpace++;
            }
            if (myNext.equals("00001010")) {
                hasNewLine++;
            }
            wordList.add(word);
            word = new ArrayList<>();
        }
        //System.out.println(wordList);
        // now wordList is ArrayList of ArrayList containing all the words in the txt file
    }

    private void countFreqWords() {
        for (int i = 0; i < wordList.size(); i++) {
            StringBuilder sb = new StringBuilder();
            for (String s : wordList.get(i)) {
                sb.append(s);
            }
            String key = sb.toString();
            // if #letters >= 2 : valid word
            if (key.length() > 8) {
                if (freqWordsMap.containsKey(key)) {
                    freqWordsMap.put(key, freqWordsMap.get(key)+1);
                } else {
                    freqWordsMap.put(key, 1);
                }
            } else {
                if (freqCharsMap.containsKey(key)) {
                    freqCharsMap.put(key, freqCharsMap.get(key)+1);
                } else {
                    freqCharsMap.put(key, 1);
                }
            }
        }
    }



    @Override
    public boolean hasNext() {
        return iter2.hasNext();
    }

    @Override
    public String next() {
        String toRtn = "";

        if (!started) {
            while (iter2.hasNext()) {
                String ltr = iter2.next();
                if (!ltr.equals("00100000") && !ltr.equals("00001010")) {
                    wordCheck += ltr;
                    toRtn = wordCheck;
                } else if (!nFreqWordsMap.containsKey(wordCheck)) {
                    System.out.println("wordCheck: " + wordCheck.length());
                    for (int k = 0; k < wordCheck.length(); k+=8) {
                        toAdd.add(wordCheck.substring(k, k+8));
                    }
                    toAdd.add(ltr);
                    wordCheck = "";
                    started = true;
                    break;
                } else {
                    toRtn = wordCheck;
                    toAdd.add(ltr);
                    wordCheck = "";
                    started = true;
                    return toRtn;
                }
            }
            // In the case that there are no more items to iterate though (iter2.hasNext = false)
            // You still want to return the final items that made up wordCheck
            if (!iter2.hasNext()) {
                if (!nFreqWordsMap.containsKey(wordCheck)) {
                    for (int k = 0; k < wordCheck.length(); k += 8) {
                        toAdd.add(wordCheck.substring(k, k+8));
                    }
                    started = true;
                    lastChars = true;
                }
                else {
                    toRtn = wordCheck;
                    return toRtn;
                }
            }
        }
        // we're started on some words already
        if (started == true) {
            toRtn = toAdd.get(h);
            h++;
            if (h == toAdd.size()) {
                h = 0;
                toAdd.clear();
                started = false;
                lastChars = false;
            }
        }
        return toRtn;
    }


    public static void main(String[] args) {
        FileFreqWordsIterator iter = new FileFreqWordsIterator("src/sample_files/kitten.txt", 1);
    }


}

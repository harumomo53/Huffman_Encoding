import java.io.*;
import java.util.*;

/**
 * Created by EunjinCho on 2016. 3. 23..
 */
public class Zipper {
    protected String myInputName;
    protected String myOutputName;
    protected File myInputDirFile;
    protected File[] listOfFiles;
    protected Boolean myIsZip;
    // a hashmap with file information
    protected HashMap<File, Number> pathAndNumber;
    protected HashMap<File, Boolean> pathAndIsDir;

    // for "unzip"
    protected List<String> outputDirSplit;


    public Zipper(String inputDirName, String outputFileName, Boolean isZip) {
        myInputName = inputDirName;
        myInputDirFile = new File(inputDirName);
        pathAndNumber = new HashMap<>();
        pathAndIsDir = new HashMap<>();
        myIsZip = isZip;
        // make sure inputDirName is directory and outputFileName ends with .zipper
        if (myIsZip) {
            if (myInputDirFile.isDirectory()) {
                if (!outputFileName.endsWith(".zipper")) {
                    outputFileName = outputFileName.concat(".zipper");
                }
            }
        }
        myOutputName = outputFileName;
        outputDirSplit = new ArrayList<>();
    }

    private void listFiles() {
        listOfFiles = myInputDirFile.listFiles();
        // add the starting directory
        if (myInputDirFile.isDirectory()) {
            pathAndNumber.put(myInputDirFile, -1);
            pathAndIsDir.put(myInputDirFile, true);
        }
        // add all the files and directories it contains
        loopFiles(listOfFiles);
    }

    // recursively find all directories of directories and files
    private void listMoreFiles(File inputF) {
        File[] listOfFiles2 = inputF.listFiles();
        loopFiles(listOfFiles2);
    }

    private void loopFiles(File[] listOfFilesArg) {
        for (File f : listOfFilesArg) {
            if (f.isDirectory()) {
                pathAndIsDir.put(f, true);
                pathAndNumber.put(f, -1);
                listMoreFiles(f);
            } else {
                pathAndIsDir.put(f, false);
            }
        }
    }

    public void countBytes() throws IOException {
        // Start encoding
        for (Map.Entry<File, Boolean> entry : pathAndIsDir.entrySet()) {
            // if it's directory, continue
            if (entry.getValue()) {
                continue;
            } else {
                File tempZip = new File("src/tempZip");
                HuffmanEncoding huffman = new HuffmanEncoding(entry.getKey().toString(), "src/tempZip");
                huffman.encode1(entry.getKey().toString(), "src/tempZip");
                int total = 0;
                System.out.println("tempZip.lengtH: " + new File("src/tempZip").length());
                pathAndNumber.put(entry.getKey(), new File("src/tempZip").length());
                //tempZip.delete();
            }
        }
    }

    public void zip() {
        File f = new File(myOutputName);
        createTableOfContents();
        // Start encoding
        for (Map.Entry<File, Boolean> entry : pathAndIsDir.entrySet()) {
            // if it's directory
            if (entry.getValue()) {
                continue;
            } else {
                HuffmanEncoding huffman = new HuffmanEncoding(entry.getKey().toString(), myOutputName);
                huffman.encode1(entry.getKey().toString(), myOutputName);

                try {
                    FileWriter fw = new FileWriter(f, true);
                    BufferedWriter bw = new BufferedWriter(fw);
                    bw.write("\n");
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void createTableOfContents() {
        File f = new File(myOutputName);
        StringBuilder strToRtn = new StringBuilder();
        int total = 0;
        for (Map.Entry<File, Number> entry : pathAndNumber.entrySet()) {
            strToRtn.append(entry.getKey());
            strToRtn.append(",");
            if (entry.getValue().intValue() == -1) {
                strToRtn.append(-1);
                strToRtn.append("\n");
                continue;
            } else {
                strToRtn.append(total);
                total += entry.getValue().intValue();
            }
            strToRtn.append("\n");
        }
        strToRtn.append("\n");

        try {
            FileWriter fw = new FileWriter(f, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(strToRtn.toString());
            bw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void unzip() {

        if (myOutputName.contains("/")) {
            outputDirSplit = Arrays.asList(myOutputName.split("/"));
        }
        // Integer is the byte size of file
        // String is the file name
        Map<Integer, String> tableOfContentsMap = new TreeMap<>();
        try {
            // Create a directory based on the table of contents on the header
            BufferedReader br = new BufferedReader(new FileReader(myInputName));
            int lenTableOfContents = 1;
            String line = br.readLine();
            // Read in Table of contents part in outputFileName and put it in the HashMap
            while (!line.equals("")) {
                StringBuilder pathSB = new StringBuilder();
                lenTableOfContents += line.length() + 1;
                List<String> list = Arrays.asList(line.split(","));

                // Remove redundancy
                if (list.get(0).contains("/")) {
                    String[] split = list.get(0).split("/");
                    for (int i = 0; i < split.length; i++) {
                        pathSB.append(split[i]);
                        pathSB.append("/");
                        if (outputDirSplit.contains(split[i])) {
                            pathSB.setLength(pathSB.length() - split[i].length() - 1);
                        }
                    }
                }


                list.set(0, myOutputName + File.separator + pathSB.toString());
                //System.out.println(list.get(0));
                File f2 = new File(list.get(0));
                // Create directory
                if (!list.get(0).contains(".")) {
                    f2.mkdir();
                    f2.mkdirs();
                }
                // add to the hashMap
                tableOfContentsMap.put(Integer.parseInt(list.get(1)), list.get(0));
                line = br.readLine();

            }
            br.close();

            FileCharIterator fc = new FileCharIterator(myInputName);
            int currByte = 0;
            // Pass the table of contents part
            for (int i = 0; i < lenTableOfContents; i++) {
                fc.next();
            }

            System.out.println(tableOfContentsMap);
            // Remove the directories in the map
            tableOfContentsMap.remove(-1);

            System.out.println(tableOfContentsMap);
            String myNext;
            // Decode
            while (!tableOfContentsMap.isEmpty()) {
                File tempUnzip = new File("src/tempUnzip");
                String f3 = tableOfContentsMap.remove(currByte); // give you the file path
                // read each files
                while (fc.hasNext() && !tableOfContentsMap.containsKey(currByte)) {
                    myNext = fc.next();
                    FileOutputHelper.writeBinStrToFile(myNext, "src/tempUnzip");
                    currByte++;
                }
                System.out.println(currByte);
                // decode that file
                HuffmanEncoding.decode("src/tempUnzip", f3);
                tempUnzip.delete();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
        String inputDirName = "src/dir1";
        String outputFileName = "src/zipOutput.zipper"; // will be the input file for "unzip"
        Zipper zipper = new Zipper(inputDirName, outputFileName, true);
        zipper.listFiles();
        zipper.countBytes();
        zipper.zip();
        Zipper unzipper = new Zipper("src/zipOutput.zipper","src/OUTPUT", false);
        unzipper.unzip();
        //HuffmanEncoding huffman = new HuffmanEncoding("src/dir1/kitten.txt", "src/dir1/outputEncode.txt");
        //huffman.encode2("src/dir1/kitten.txt", "src/dir1/outputEncode.txt", 1);

    }
}

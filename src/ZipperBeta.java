import java.io.*;
import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by EunjinCho on 2016. 3. 24..
 */
public class ZipperBeta {
    String myInputName;
    String myOutputName;
    File myInputFile;

    // ================== For Zipping ==================
    HashMap<String, Boolean> isDirectoryMap;
    ArrayList<String> fileNameArr;
    ArrayList<File> listOfFiles;
    private File temp;
    private File destination;

    HashMap<String, Number> pathBytes;
    HashMap<String, Number> fileBytes;

    // ================== For Unzipping ==================
    private File tempUn;

    public static void main(String[] args) {
        // ================== Zipping ==================
        String inputDirectory = "src/dir1";
        String outputFile = "src/zipOutput.txt";
        ZipperBeta zipper = new ZipperBeta(inputDirectory, outputFile, true);
        // create a list of all files inside the directory
        zipper.listFiles(inputDirectory);
        zipper.addFileInfo();
        zipper.countBytes();
        zipper.writeTables();
        zipper.writeEncoded(zipper.temp, zipper.destination);
        zipper.readLine();
        zipper.temp.delete();

        // ================= Un-Zipping =================
        String inputFile = "src/zipOutput.txt";
        String outputDir = "src/dirTestOutput";
        ZipperBeta unzipper = new ZipperBeta(inputFile, outputDir, false);
        unzipper.unzip();
    }

    private void readLine() {
    }


    public ZipperBeta(String inputName, String outputName, Boolean isZip) {
        myInputName = inputName;
        myInputFile = new File(inputName);
        // concatenate the name with .zipper
        /*if (isZip) {
            if (myInputFile.isDirectory()) {
                if (!outputName.endsWith(".zipper")) {
                    outputName = outputName.concat(".zipper");
                }
            }
        }*/
        myOutputName = outputName;
        if (isZip) {
            isDirectoryMap = new HashMap<>();
            fileNameArr = new ArrayList<>();
            listOfFiles = new ArrayList<>();
            temp = new File("src/tempZip.txt");
            destination = new File("src/zipOutput.txt");
            fileBytes = new HashMap<>();
            pathBytes = new HashMap<>();
        }
        if (!isZip) {
            tempUn = new File("src/tempUnzip.txt");
        }
    }


    private void unzip() {
        try {
            HashMap<Integer, String> tableOfContentsMap = new HashMap<Integer, String>();
            BufferedReader br = new BufferedReader(new FileReader(this.myInputFile));

            // Create directories and files given the table of contents
            int start = 1;
            String line = br.readLine();
            while (!line.equals("")) {
                start += line.length() + 1;
                List<String> list = Arrays.asList(line.split(","));
                list.set(0, this.myOutputName + File.separator + list.get(0));
                File f = new File(list.get(0));
                if (!list.get(0).contains(".")) {
                    f.mkdir();
                    f.mkdirs();
                }
                tableOfContentsMap.put(Integer.parseInt(list.get(1)), list.get(0));
                line = br.readLine();
            }
            br.close();

            // Pass the table of contents above
            FileCharIterator fc = new FileCharIterator(myInputName);
            int currByte = 0;
            for (int i = 0; i < start; i++) {
                fc.next();
            }
            tableOfContentsMap.remove(-1);

            while (!tableOfContentsMap.isEmpty()) {
                String file = tableOfContentsMap.remove(currByte);
                //System.out.println("file: " + file);
                while (fc.hasNext() && !tableOfContentsMap.containsKey(currByte)) {
                    String myNext = fc.next();
                    System.out.println("myNext: " + myNext);
                    FileOutputHelper.writeBinStrToFile(myNext, "src/tempUnzip.txt");
                    currByte++;
                }
                System.out.println("currByte: " + currByte);
                HuffmanEncoding huffman = new HuffmanEncoding("src/tempUnzip.txt", file);
                huffman.decode("src/tempUnzip.txt", file);
                tempUn.delete();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void writeEncoded(File myInput, File myDest) {
        StringBuilder strToRtn = new StringBuilder();
        FileCharIterator fc = new FileCharIterator(myInput.toString());
        while (fc.hasNext()) {
            strToRtn.append(fc.next());
        }
        FileOutputHelper.writeBinStrToFile(strToRtn.toString(), myDest.toString());
    }


    private void writeTables() {
        try {
            PrintWriter out = new PrintWriter(new FileWriter(this.myOutputName, true));
            for (Map.Entry<String, Number> entry : pathBytes.entrySet()) {
                String path = entry.getKey();
                Number pathByte = entry.getValue();
                out.println(path + "," + pathByte);
            }
            out.println();
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create an arraylist of all files and directories inside the input
     */
    public void listFiles(String input) {
        File thisF = new File(input);
        listOfFiles.add(thisF);
        File[] listOfF = thisF.listFiles();
        for (File f : listOfF) {
            if (f.isDirectory()) {
                listOfFiles.add(f);
                listFiles(f.toString());
            } else {
                listOfFiles.add(f);
            }
        }
    }

    /**
     * Add information about the file (isDirectory, name)
     */
    public void addFileInfo() {
        for (File f : listOfFiles) {
            if (f.isDirectory()) {
                isDirectoryMap.put(f.toString(), true);
                fileNameArr.add(f.toString());
            } else {
                isDirectoryMap.put(f.toString(), false);
                fileNameArr.add(f.toString());
            }
        }
    }

    private void countBytes() {
        int i = 0;
        long prevPathByte = 0;
        long prevFileByte = 0;
        long currDestLength = 0;
        for (int count = 0; count < fileNameArr.size(); count++) {
            String fileName = fileNameArr.get(count);
            // if it's a file
            if (!isDirectoryMap.get(fileName)) {
                String[] arguments = new String[3];
                arguments[0] = fileNameArr.get(count); // filename
                arguments[1] = "src/tempZip.txt";

                if (i == 0) {
                    currDestLength = 0;
                    System.out.println("currDestLength0: " + 0);
                } else {
                    currDestLength = prevPathByte + prevFileByte;
                    System.out.println("currDestLength1: " + currDestLength);
                }

                pathBytes.put(arguments[0], currDestLength);
                prevPathByte = currDestLength;
                System.out.println("Save to pathBytes: " + currDestLength);

                HuffmanEncoding.encode1(arguments[0], arguments[1]);
                System.out.println("temp.length(): " + temp.length());
                if (i == 0) {
                    fileBytes.put(arguments[0], temp.length() - currDestLength );
                    prevFileByte = temp.length() - currDestLength;
                    System.out.println("Save to fileBytes0: " + (temp.length() - currDestLength));
                } else {
                    fileBytes.put(arguments[0], temp.length() - currDestLength);
                    prevFileByte = temp.length() - currDestLength;
                    System.out.println("Save to fileBytes1: " + (temp.length() - currDestLength));
                }
                System.out.println();
                try {
                    PrintWriter out = new PrintWriter(new FileWriter("src/tempZip.txt", true));
                    //out.println();
                    out.close();
                    i++;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                long bytes = -1;
                pathBytes.put(fileNameArr.get(count), bytes);
            }
        }
        try {
            PrintWriter out = new PrintWriter(new FileWriter("src/tempZip.txt", true));
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

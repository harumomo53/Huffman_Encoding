import java.util.*;

/**
 * Created by EunjinCho on 2016. 3. 19..
 */
public class BinaryTree {
    protected TreeNode myRoot;
    Map<String, Integer> myStrFreq;
    Map<TreeNode, Integer> advancedFreqMap;

    public BinaryTree(Map<String, Integer> countedFreq) {
        myRoot = null;
        myStrFreq = countedFreq;
        advancedFreqMap = new HashMap<>();
    }

    public BinaryTree(TreeNode t) {
        myRoot = t;
    }

    public void setRoot(TreeNode t) {
        myRoot = t;
    }


    public void convertMap() {
        for (Map.Entry<String, Integer> entry : myStrFreq.entrySet()) {
            String myKey = entry.getKey();
            Integer myVal = entry.getValue();
            TreeNode myNode = new TreeNode(myKey, myVal);
            advancedFreqMap.put(myNode, myNode.myWeight);
        }
        TreeNode EOF = new TreeNode("EOF", -1);
        advancedFreqMap.put(EOF, EOF.myWeight);
    }


    private Map.Entry<TreeNode,Integer> getSmallest(Map<TreeNode, Integer> newFreq) {
        int min = Integer.MAX_VALUE;
        Map.Entry<TreeNode, Integer> min_entry = null;
        for (Map.Entry<TreeNode, Integer> entry : newFreq.entrySet()) {
            if (min > entry.getValue()) {
                min = entry.getValue();
                min_entry = entry;
            }
        }
        return min_entry;
    }

    public TreeNode generateTree() {
        if (advancedFreqMap.size() > 1) {
            // Find 2 nodes with the smallest frequencies
            Map.Entry<TreeNode, Integer> smallest = getSmallest(advancedFreqMap);
            advancedFreqMap.remove(smallest.getKey());
            Map.Entry<TreeNode, Integer> secondSmallest = getSmallest(advancedFreqMap);
            advancedFreqMap.remove(secondSmallest.getKey());

            // Create a new node with smallest and second smallest nodes as left and right child
            TreeNode added = new TreeNode(smallest.getKey(), secondSmallest.getKey());
            myRoot = added;
            advancedFreqMap.put(added, added.myWeight);
            generateTree();
        }
        return myRoot;
    }



    public HashMap<String, String> generateMapping() {
        /*
        Construct a table mapping characters to codewords
        */
        HashMap<String, String> chrMap = new HashMap<>();
        Stack<TreeNode> myStack = new Stack<>();
        Stack<String> myPath = new Stack<>();
        myStack.push(myRoot);
        myPath.push("");
        TreeNode node;
        String path;

        while (!myStack.isEmpty()) {
            node = myStack.pop();
            path = myPath.pop();
            if (node.myString != null) {
                chrMap.put(node.myString, path);
            } else {
                // because path is Stack, last in first out
                myStack.push(node.myLeft);
                myStack.push(node.myRight); // out first
                myPath.push(new String(path + "0"));
                myPath.push(new String(path + "1"));
            }
        }
        return chrMap;
    }

    public void print() {
        if (myRoot != null) {
            myRoot.print(1);
        }
    }


    /**
     * TreeNode Class to use for binary tree
     */
    public static class TreeNode {
        public String myString;
        public TreeNode myLeft;
        public TreeNode myRight;
        public int myWeight;

        public TreeNode(String str, int weight) {
            myString = str;
            myLeft = myRight = null;
            myWeight = weight;
        }

        public TreeNode() {
            myString = null;
            myLeft = myRight = null;
            myWeight = 0;
        }

        public TreeNode(TreeNode left, TreeNode right) {
            myString = null;
            myLeft = left;
            myRight = right;
            myWeight = left.myWeight + right.myWeight;
        }

        private static final String indent1 = "    ";

        public void print(int indent) {
            if (myRight != null) {
                myRight.print(indent+1);
            }
            println(myString, indent, myWeight);
            if (myLeft != null) {
                myLeft.print(indent+1);
            }
        }

        public void println(Object obj, int indent, int weight) {
            for (int k = 0; k < indent; k++) {
                System.out.print(indent1);
            }
            System.out.println(obj + "(" + weight + ")" );
        }
    }

}

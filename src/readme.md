Huffman Encoding
===
Go to ```src```  and you'll find necessary files to operate huffman encoding.

### Encode
* To encode using normal Iterator: ```java HuffmanEncoding encode [input file] [destination file]```
* To encode using advanced Iterator: ```java HuffmanEncoding encode2 [input file] [destination directory] n```

### Decode
* To decode: ```java HuffmanEncoding decode [input file] [destination file]```

### ZipperBeta
* To zip: ```java ZipperBeta zipper [input directory name] [output file name]```
* To unzip: ```java ZipperBeta unzipper [input file name] [output directory name]```

